/////////////////////////////////////////////////////////////////////
// jQuery for page scrolling feature - requires jQuery Easing plugin
/////////////////////////////////////////////////////////////////////

$('.page-scroll').bind('click', function(event) {
    var $anchor = $(this);
    $('html, body').stop().animate({
        scrollTop: $($anchor.attr('href')).offset().top -64
    }, 1500, 'easeInOutExpo');
    event.preventDefault();
});



// //////////////////////////////////////////////////////////////////////
// On-Scroll Animated Header: https://github.com/codrops/AnimatedHeader
// //////////////////////////////////////////////////////////////////////

var cbpAnimatedHeader = (function() {

    var docElem = document.documentElement,
        header = document.querySelector( '.navbar-fixed-top' ),
        didScroll = false,
        changeHeaderOn = 10;

    function init() {
        window.addEventListener( 'scroll', function( event ) {
            if( !didScroll ) {
                didScroll = true;
                setTimeout( scrollPage, 250 );
            }
        }, false );
    }

    function scrollPage() {
        var sy = scrollY();
        if ( sy >= changeHeaderOn ) {
            classie.add( header, 'navbar-shrink' );
        }
        else {
            classie.remove( header, 'navbar-shrink' );
        }
        didScroll = false;
    }

    function scrollY() {
        return window.pageYOffset || docElem.scrollTop;
    }

    init();

})();



// ////////////////////////////////////////////
// Highlight the top nav as scrolling occurs
// ////////////////////////////////////////////

$('body').scrollspy({
    target: '.navbar',
    offset: 65
})



// /////////////////////////////////////////
// Display loading image while page loads
// /////////////////////////////////////////

// Wait for window load
$(window).load(function() {
    // Animate loader off screen
    // $(".page-loader").fadeOut("slow");
	getLocation();
});



// //////////////////////////////////////////////////
// OWL Carousel: http://owlgraphic.com/owlcarousel
// //////////////////////////////////////////////////

// Intro text carousel
$("#owl-intro-text").owlCarousel({
    singleItem : true,
    autoPlay : 6000,
    stopOnHover : true,
    navigation : false,
    navigationText : false,
    pagination : true
})


// Partner carousel
$("#owl-partners").owlCarousel({
    items : 4,
    itemsDesktop : [1199,3],
    itemsDesktopSmall : [980,2],
    itemsTablet: [768,2],
    autoPlay : 5000,
    stopOnHover : true,
    pagination : false
})

// Testimonials carousel
$("#owl-testimonial").owlCarousel({
    singleItem : true,
    pagination : true,
    autoHeight : true
})


// //////////////////////////////////////////////////////////////////
// Stellar (parallax): https://github.com/markdalgleish/stellar.js
// //////////////////////////////////////////////////////////////////

$.stellar({
    // Set scrolling to be in either one or both directions
    horizontalScrolling: false,
    verticalScrolling: true,
});



// /////////////////////////////////////////////////////////
// WOW animation scroll: https://github.com/matthieua/WOW
// /////////////////////////////////////////////////////////

new WOW().init();



// //////////////////////////////////////////////////////////////////////////////////////////
// Counter-Up (requires jQuery waypoints.js plugin):
// https://github.com/bfintal/Counter-Up
// //////////////////////////////////////////////////////////////////////////////////////////

$('.counter').counterUp({
    delay: 10,
    time: 2000
});



// //////////////////////////////////////////////////////////////////////////////////////////
// Isotop Package
// //////////////////////////////////////////////////////////////////////////////////////////
$(window).load(function() {
$('.portfolio_menu ul li').click(function(){
	$('.portfolio_menu ul li').removeClass('active_prot_menu');
	$(this).addClass('active_prot_menu');
});

var $container = $('#portfolio');
$container.isotope({
  itemSelector: '.col-sm-4',
  layoutMode: 'fitRows'
});
$('#filters').on( 'click', 'a', function() {
  var filterValue = $(this).attr('data-filter');
  $container.isotope({ filter: filterValue });
  return false;
});
});



// //////////////////////////////////
// Scroll to top button
// //////////////////////////////////////

// Check to see if the window is top if not then display button
$(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
        $('.scrolltotop').fadeIn();
    } else {
        $('.scrolltotop').fadeOut();
    }
});

// Click event to scroll to top
$('.scrolltotop').click(function(){
    $('html, body').animate({scrollTop : 0}, 1500, 'easeInOutExpo');
    return false;
});



// //////////////////////////////////////////////////////////////////
// Close mobile menu when click menu link (Bootstrap default menu)
// //////////////////////////////////////////////////////////////////

$(document).on('click','.navbar-collapse.in',function(e) {
    if( $(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle' ) {
        $(this).collapse('hide');
    }
});


// var x = document.getElementById("demo");
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        showError("Geolocation is not supported by this browser.");
    }
}
function showPosition(position) {
  
	var lat=readCookie("iLat")
	var lng=readCookie("iLng")
	
	if(lat!=null && lng!=null)
		getWeatherInformation(lat,lng)
	else
		 getWeatherInformation(position.coords.latitude,position.coords.longitude)
	
   
}



function getWeatherInformation(lat,lng){

	$.ajax({
		url:"api/show/"+lat+"/"+lng,
		type:"GET",
		dataType:"json",
		beforeSend:function(){},
		complete:function(){ $(".page-loader").fadeOut();},
		success:function(data){
			// alert(data)
			createCookie("iLat",lat,7);
			createCookie("iLng",lng,7);
			populateField(data)
		},
		error:function(f,g,e){
			alert(e)
			}
		
	})
}

function populateField(weatherObject){
	
	$("#result").click();
	
	if(weatherObject.weatherObservation!=undefined){
		// alert(weatherObject.countryCode)
		$("#errorMessageWrapper").hide();
		$("#errorMessage").html("");
		$("#weatherInfoWrapper").show();
		
		$("#stationName").html(weatherObject.weatherObservation.stationName)
		$("#cc").html(weatherObject.weatherObservation.countryCode)
		$("#temp").html(weatherObject.weatherObservation.temperature)
		$("#clouds").html(weatherObject.weatherObservation.clouds)
		$("#dewPoint").html(weatherObject.weatherObservation.dewPoint)
		$("#windSpeed").html(weatherObject.weatherObservation.windSpeed)
		$("#windDirection").html(weatherObject.weatherObservation.windDirection)
		$("#weatherCondition").html(weatherObject.weatherObservation.weatherCondition)
		$("#humidity").html(weatherObject.weatherObservation.humidity)
		$("#lng").html(weatherObject.weatherObservation.lng)
		$("#lat").html(weatherObject.weatherObservation.lat)
		$("#datetime").html(weatherObject.weatherObservation.datetime)
		$("#weatherCondition").html(weatherObject.weatherObservation.weatherCondition)
	}else{
		$("#weatherInfoWrapper").hide();
		$("#errorMessageWrapper").show();
		$("#errorMessage").html(weatherObject.status.message);
	}
	
}


$("#finderBtn").click(function(){
	var address=$("#txtLocation").val();
// alert(address)
	if(address==""){
		alert("Please enter a location");
		return
	}
// alert(address)
	
	// geocode
	var url="api/geoCode/"+encodeURI(address);
	
	$.ajax({
		url:url,
		type:"GET",
		dataType:"json",
		beforeSend:function(){$(".page-loader").fadeIn("slow");},
		complete:function(){$(".page-loader").fadeOut(); },
		success:function(data){
			
			switch (data.status){
				case "OK":
					var lat=data.results[0].geometry.location.lat;
					var lng=data.results[0].geometry.location.lng;
					getWeatherInformation(lat,lng);
					break;
				case "ZERO_RESULTS":
					showError("No Result found!");
					break;
				case "OVER_QUERY_LIMIT":
					showError("Quota Exceeded");
					break;
			}
//			if(data.status!="OK"){
//				showError(data.error_message)
//			}else{
//				if(data.results.length==0){
//					showError("No result found for "+address)
//				}else{
//				var lat=data.results[0].geometry.location.lat;
//				var lng=data.results[0].geometry.location.lng;
//				getWeatherInformation(lat,lng)
//				}
//				
//			}

		},
		error:function(e,g,f){
			 $(".page-loader").fadeOut();
			 	showError(f)
			}
		
	})
	
})

function showError(msg){
	$("#weatherInfoWrapper").hide();
	$("#errorMessageWrapper").show();
	$("#errorMessage").html(msg);
}

function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}