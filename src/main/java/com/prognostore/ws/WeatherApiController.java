package com.prognostore.ws;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.WebRequest;

@Controller
@RequestMapping(value="/api")
public class WeatherApiController {

	private static final Logger logger = LoggerFactory.getLogger(WeatherApiController.class);

	//AIzaSyAAnvI-0XObvvZnHNU0s7IulAYqNzYTDck
	@RequestMapping(value="/show/{lat}/{lng}", method=RequestMethod.GET)
	public @ResponseBody String show(@PathVariable String lat,
			@PathVariable String lng,WebRequest request) {
		
		if(!isAjaxRequest(request)){
			return "Invalid Request";
		}
		RestTemplate restTemplate = new RestTemplate();
		StringBuilder builder=new StringBuilder();
		builder.append("http://api.geonames.org/findNearByWeatherJSON?")
		.append("lat=")
		.append(lat)
		.append("&lng=")
		.append(lng)
		.append("&username=prognotest");
		String url=builder.toString();
		ResponseEntity<String> str=restTemplate.getForEntity(url, String.class);
		String response=str.getBody();
		
		logger.info("Latitude: "+lat+" Longitude: "+lng+" "+response);
		return response;//(user.getUserId());

	}
	
	@RequestMapping(value="/geoCode/{address}", method=RequestMethod.GET)
	public @ResponseBody String geoCode(@PathVariable String address,WebRequest request) {
		
		if(!isAjaxRequest(request)){
			return "Invalid Request";
		}
		RestTemplate restTemplate = new RestTemplate();
		StringBuilder builder=new StringBuilder();
		builder.append("https://maps.googleapis.com/maps/api/geocode/json?")
		.append("address=")
		.append(address)
		.append("&key=AIzaSyAAnvI-0XObvvZnHNU0s7IulAYqNzYTDck");
		String url=builder.toString();
		try{
			ResponseEntity<String> str=restTemplate.getForEntity(url, String.class);
			String response=str.getBody();
			logger.info("Addess: "+response);
			return response;
		}catch(RestClientException exception){
			logger.info("Exception: "+exception.toString());
			return "{ \"error_message\" : \""+exception.getMessage()+"\", \"results\" : [], \"status\" : \"HOST_NOT_FOUND\" }";
		}
		

	}
	private static boolean isAjaxRequest(WebRequest webRequest) {
		String requestedWith = webRequest.getHeader("X-Requested-With");
		return requestedWith != null ? "XMLHttpRequest".equals(requestedWith) : false;
	}
}
