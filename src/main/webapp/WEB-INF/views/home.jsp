<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@ page session="true"%>
<!doctype html>
<html lang="en-US">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>PrognoStore Weather Station</title>
<meta name="description"
	content="Weather Information">
<meta name="keywords"
	content="Weather Information" />
<c:set var="req" value="${pageContext.request}" />
<base
	href="${fn:replace(req.requestURL, req.requestURI, req.contextPath)}/" />

<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Google Fonts  -->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,500'
	rel='stylesheet' type='text/css'>
<!-- Body font -->
<link href='http://fonts.googleapis.com/css?family=Lato:300,400'
	rel='stylesheet' type='text/css'>
<!-- Navbar font -->

<!-- Libs and Plugins CSS -->
<link rel="stylesheet"
	href="resources/inc/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="resources/inc/animations/css/animate.min.css">
<link rel="stylesheet"
	href="resources/inc/font-awesome/css/font-awesome.min.css">
<!-- Font Icons -->
<link rel="stylesheet"
	href="resources/inc/owl-carousel/css/owl.carousel.css">
<link rel="stylesheet"
	href="resources/inc/owl-carousel/css/owl.theme.css">

<!-- Theme CSS -->
<link rel="stylesheet" href="resources/css/reset.css">
<link rel="stylesheet" href="resources/css/style.css">
<link rel="stylesheet" href="resources/css/mobile.css">

<!-- Skin CSS -->
<link rel="stylesheet" href="resources/css/skin/cool-gray.css">


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

</head>

<body data-spy="scroll" data-target="#main-navbar">
	<div class="page-loader"></div>
	<!-- Display loading image while page loads -->
	<div class="body">

		<!--========== BEGIN HEADER ==========-->
		<header id="header" class="header-main">

			<!-- Begin Navbar -->
			<nav id="main-navbar" class="navbar navbar-default navbar-fixed-top"
				role="navigation">
				<!-- Classes: navbar-default, navbar-inverse, navbar-fixed-top, navbar-fixed-bottom, navbar-transparent. Note: If you use non-transparent navbar, set "height: 98px;" to #header -->

				<div class="container">

					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed"
							data-toggle="collapse"
							data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
						<a class="navbar-brand page-scroll" href="index.html">Unika</a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse"
						id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right hidden">
							<li><a class="page-scroll" id="result" href="#about-section">About</a></li>
						</ul>
					</div>
					<!-- /.navbar-collapse -->
				</div>
				<!-- /.container -->
			</nav>
			<!-- End Navbar -->

		</header>
		<!-- ========= END HEADER =========-->




		

		<!-- Begin about section -->
		<section id="about-section" class="page bg-style1">



			<!-- End rotate box-1 -->

			<div class="extra-space-l"></div>

			<!-- Begin page header-->
			<div class="page-header-wrapper">
				<div class="container">
					<div class="page-header text-center wow fadeInUp"
						data-wow-delay="0.3s">
						<h4>Weather Information</h4>
					</div>
				</div>
			</div>
			<!-- End page header-->

			<!-- Begin Our Skills -->
			<div class="our-skills">
				<div class="container">
					<div class="row">

						<div class="col-lg-12">
							<div class="well well-lg">
								<div class="input-group">
									<input type="text" class="form-control" id="txtLocation"
										placeholder="Enter a location e.g Osogbo"> <span
										class="input-group-btn">
										<button class="btn btn-default" id="finderBtn" type="button">Submit</button>
									</span>
								</div>
								<!-- /input-group -->
							</div>

						</div>
						<!-- /.col-lg-6 -->
					</div>
					<!-- /.row -->
					<div class="row" id="weatherInfoWrapper">
					
					<div class="well">
						<table class="table table-striped fa-1x">
							<tr>
								<td class="text-bold">Clouds</td>
								<td id="clouds"></td>
							</tr>
							<tr>
								<td class="text-bold">Dew Point</td>
								<td id="dewPoint"></td>
							</tr>
							<tr>
								<td class="text-bold">Country Code</td>
								<td id="cc"></td>
							</tr>


							<tr>
								<td class="text-bold">Temperature</td>
								<td id="temp"></td>
							</tr>
							<tr>
								<td class="text-bold">Humidity</td>
								<td id="humidity"></td>
							</tr>

							<tr>
								<td class="text-bold">Station Name</td>
								<td id="stationName"></td>
							</tr>

							<tr>
								<td class="text-bold">Wind Direction</td>
								<td id="windDirection"></td>
							</tr>
							<tr>
								<td class="text-bold">Wind Speed</td>
								<td id="windSpeed"></td>
							</tr>
							<tr>
								<td class="text-bold">Weather Condition</td>
								<td id="weatherCondition"></td>
							</tr>
							<tr>
								<td class="text-bold">Latitude</td>
								<td id="lat"></td>
							</tr>
							<tr>
								<td class="text-bold">Longitude</td>
								<td id="lng"></td>
							</tr>

							<tr>
								<td class="text-bold">Observation Time</td>
								<td id="datetime"></td>
							</tr>

						</table>
					</div>

						




					</div>
					<!-- /.row -->

					<div class="row" id="errorMessageWrapper">
						<div class="alert alert-warning fa-2x" id="errorMessage"></div>
					</div>
				</div>
				<!-- /.container -->
			</div>
			<!-- End Our Skill -->
		</section>
		<!-- End about section -->











		<!-- Begin footer -->
		<footer class="text-off-white">


			<div class="footer">
				<div class="container text-center wow fadeIn" data-wow-delay="0.4s">
					<p class="copyright">
						Copyright &copy; 2016 - Designed By Oluwafemi Fagbemi
					</p>
				</div>
			</div>

		</footer>
		<!-- End footer -->

		<a href="#" class="scrolltotop"><i class="fa fa-arrow-up"></i></a>
		<!-- Scroll to top button -->

	</div>
	<!-- body ends -->




	<!-- Plugins JS -->
	<script src="resources/inc/jquery/jquery-1.11.1.min.js"></script>
	<script src="resources/inc/bootstrap/js/bootstrap.min.js"></script>
	<script src="resources/inc/owl-carousel/js/owl.carousel.min.js"></script>
	<script src="resources/inc/stellar/js/jquery.stellar.min.js"></script>
	<script src="resources/inc/animations/js/wow.min.js"></script>
	<script src="resources/inc/waypoints.min.js"></script>
	<script src="resources/inc/isotope.pkgd.min.js"></script>
	<script src="resources/inc/classie.js"></script>
	<script src="resources/inc/jquery.easing.min.js"></script>
	<script src="resources/inc/jquery.counterup.min.js"></script>
	<script src="resources/inc/smoothscroll.js"></script>

	<!-- Theme JS -->
	<script src="resources/js/theme.js"></script>

</body>


</html>
